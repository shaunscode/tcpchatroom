package client;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;

import common.Connection;
import common.Command;

public class Model {

	private ArrayList<DisplayMessage> messages = new ArrayList<DisplayMessage>();
	private ArrayList<String> connectedClients = new ArrayList<String>();
	
	public ArrayList<String> getConnectedClients() {
		return connectedClients;
	}


	public void setConnectedClients(ArrayList<String> connectedClients) {
		this.connectedClients = connectedClients;
	}


	public void addMessage (String s) {
		messages.add(new DisplayMessage(s));
	}
	
	
	public String getMessages () {
		Collections.sort(messages);
		StringBuilder sb = new StringBuilder();
		for (DisplayMessage msg: messages) {
			sb.append(msg.message + "\n");
		}		
		return sb.toString();
	}
	
	
	private class DisplayMessage implements Comparable<DisplayMessage>{
		
		private String message;
		private long createdTime;
		
		public DisplayMessage (String msg) {
			this.message = msg;
			this.createdTime = System.currentTimeMillis();
		}
		
		public String getMessage () {
			return this.message;
		}

		@Override
		public int compareTo(DisplayMessage other) {
			return (this.createdTime < other.createdTime? -1: 1);
		}
		
		
		
	}
}
