/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;



/**
 *
 * @author sac248
 */
public class RunClient {
    
    public static void main(String[] args)  {
    	
    	try {
			Client client = new Client ();
			client.runMainLoop();
		} catch (Exception e) {
			System.out.println("There was an exception running the client program: " + e.getMessage());
			e.printStackTrace();
		}
    	
    	System.out.println("Client program has ended.");
    	
    }    
 
}
