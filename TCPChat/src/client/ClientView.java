package client;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class ClientView extends JFrame {

	private GUIConnect connectionView;
	private GUIMain mainView;


	public ClientView(Object clientLock) throws InterruptedException {

		super("Client GUI");
		
		this.connectionView = new GUIConnect(clientLock);
		this.mainView = new GUIMain();

		this.setLocation(100, 100);
		this.setSize(600, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setContentPane(connectionView);
		this.setVisible(true);
	}

	public void showMainView() {
		remove(connectionView);
		setContentPane(mainView);
		revalidate();
		repaint();
		
	}
	
	public void showConnectView() {
		remove(mainView);
		setContentPane(connectionView);
		revalidate();
		repaint();
		
	}

	public String getClientName () {
		return connectionView.getName();
	}
	
	public String getServerAddress () {
		return connectionView.getServerAddress();
	}

	public int getServerPort () {
		return Integer.parseInt(connectionView.getServerPort());
	}
	
	public ArrayList<String> getClientInputs() {
		return mainView.getUserInputs();
	}

	public void updateChatArea(String msgs) {
		mainView.updateChatArea(msgs);
	}

}
