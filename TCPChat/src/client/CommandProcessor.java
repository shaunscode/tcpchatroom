package client;

import common.CommandProducer;
import java.util.ArrayList;
import common.Command;

public class CommandProcessor extends CommandProducer {

	Model model;
	
	public CommandProcessor (Model model) {
		this.model = model;
	}
	
	protected void processCommand (Command cmd) {
		//System.out.println("ServerMessagesProcessor.processCommand: Recieved msg: " + msg + " at " + System.currentTimeMillis());
		switch (cmd.type) {
		case PING: 			
			break;
		case PRINT:
			model.addMessage(cmd.args);
			break;
		case UPDATE_CONNECTED:
			updateConnected(cmd.args);
			break;
		default:
			System.out.println("Client.processMessage(): Unhandled msg type (hit default in switch statement): " + cmd);
			
			break;
		}
		
	}
		
	private void updateConnected (String connectedClients) {
		String[] names = connectedClients.split(",");
		ArrayList<String> namesArr = new ArrayList<String>();
		for (String name: names) {
			namesArr.add(name);
		}
		model.setConnectedClients(namesArr);
	}
	
	public void processCommands (ArrayList<Command> msgs) {		
		//process them all 
		for (Command msg: msgs) {
			processCommand(msg);
		}			
	}
}
