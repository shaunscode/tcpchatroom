package client;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import common.Command;
import common.CommandType;
import common.Connection;
import common.Constants;
import common.NetProfile;
import common.Network;

public class Client {

	//private Connection connection;
	private ArrayList<Command> newCommandsToSend;
	private Model model;
	private ClientView view;
	private CommandProcessor commands;

	private NetProfile server;
	
	private long lastPing = System.currentTimeMillis();
	private long lastAck = System.currentTimeMillis();
	private Object clientLock = new Object();
	
	public Client() throws ClassNotFoundException, IOException, InterruptedException {
		
		//used to block this thread until the client view notifies that this thread that the clientview thread has the server details ready
		
		view = new ClientView(clientLock);
			
		newCommandsToSend = new ArrayList<Command>();
		model = new Model();
		commands = new CommandProcessor(model);
		
		lastPing = System.currentTimeMillis();
		lastAck = System.currentTimeMillis();
		
		runMainLoop();
	}
	
	
	
	public void runMainLoop () {
		
		while (true) {
			
			view.showConnectView();
			
			synchronized (clientLock) {
				try {
					clientLock.wait();
				} catch (InterruptedException e) {}
			}
			
			try {
				connectToServer();
			} catch (IOException e1) {
				continue;
			}
			view.showMainView();
			
			while (server.isConnected()) {
	
				processUserInput();
							
				ArrayList<Command> newMessages = server.getNewCommands();
				commands.processCommands(newMessages);
				ArrayList<Command> messageResults =  commands.getCommandsToDispatch();
				newCommandsToSend.addAll(messageResults);
				sendCommands(newCommandsToSend);
				
				try {
					checkConnection();
				} catch (IOException e) {
					;
				}
	
				
				try {
					Thread.sleep(Constants.CLIENT_REFRESH_RATE);
				} catch (InterruptedException e) {}	//throws InterruptedException
				view.updateChatArea(model.getMessages());
			}
		}
	}
	
	public void connectToServer () throws IOException {
		Connection connection = getConnectionToServer(view.getServerAddress(), view.getServerPort());	//throws ClassNotFoundException and IOException
		server = new NetProfile(connection);
	}
	
	private void checkConnection () throws IOException {

		long currentTime = System.currentTimeMillis();
		if (currentTime > lastPing + Constants.PING_FREQUENCY) {
			server.sendNewCommand(new Command(CommandType.PING));
			lastPing = currentTime;
		}

		if (currentTime > (lastAck + Constants.SERVER_TIMEOUT_THRESHHOLD)) {
			throw new IOException("Client has not received ping from server for more than "
					+ (Constants.SERVER_TIMEOUT_THRESHHOLD / 1000) + " seconds");
		}
		
		if (!server.isConnected()) {
			throw new IOException("Client connection is no longer running...");
		}
		
	}

	private void processUserInput() {
		ArrayList<String> userInputs = view.getClientInputs();
		for (String input : userInputs) {
			Command cmd = null;
			try {
				cmd = getCommandFromUserInput(input);
			} catch (RuntimeException e) {
				System.out.println("RuntimeException trying to create Command from user input: " + e.getMessage());
				continue;
			}
			newCommandsToSend.add(cmd);
		}

	}
	
	private Command getCommandFromUserInput (String input) {
		
		if (input.startsWith("/")) {
			
			String[] tokens = input.split(" ");
			String cmdString = tokens[0].substring(1, tokens[0].length()).toUpperCase();
			CommandType cmdType = CommandType.valueOf(cmdString);			
			String cmdArgs = input.substring(cmdString.length() + 1, input.length()).trim();
			return new Command(cmdType, cmdArgs);
			
			/*
			int index = input.indexOf(" ");
			String cmdString = input.substring(1, index).trim().toUpperCase();
			String cmdArgs = input.substring(index, input.length()).trim();
			CommandType cmdType = CommandType.valueOf(cmdString);
			return new Command(cmdType, cmdArgs);*/
		}
		
		return new Command(CommandType.CL_INPUT, input);
		
	}
	
	private void sendCommands (ArrayList<Command> newCommandsToSend) {
		synchronized (newCommandsToSend) {
			for (Command cmd : newCommandsToSend) {
				server.sendNewCommand(cmd);
			}
			newCommandsToSend.clear();
		}
	}

	private Connection getConnectionToServer(String address, int port) throws IOException  {

		// get the socket to the server
		Socket socketToServer = new Socket(address, port);	 //throws IOException

		// get connection to return
		Connection connection = new Connection("Client conection", socketToServer);	 //throws IOException

		// return connection
		return connection;
	}

}
