package client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class GUIMain extends JPanel {


	private ArrayList<String> userInputs;
	private JTextField userInputArea;
	private JTextArea chatArea;
	private JButton sendButton;
	private JScrollPane scrollPane;
	
	public GUIMain () {
		
		userInputArea = new JTextField(40);
		userInputs = new ArrayList<String>();
		chatArea = new JTextArea();
		
		//setup layout
		this.setLayout(new BorderLayout());				
		
		this.add(new JLabel("Chat client"),BorderLayout.NORTH);
		this.add(createChatArea(), BorderLayout.CENTER);
		this.add(createControlArea(), BorderLayout.SOUTH);
		
	}
	
	private JPanel createControlArea () {
		//make the lower panel for text input and send button
		JPanel lowerPanel = new JPanel();		
		sendButton = new JButton("     Send     ");
		lowerPanel.add(userInputArea);
		lowerPanel.add(sendButton);		
		attachSendAction(sendButton);
		attachKeyListener(userInputArea);
		return lowerPanel;
	}
	
	private JScrollPane createChatArea () {
		
		chatArea.setLineWrap(true);
		//chatArea.setPreferredSize(new Dimension(450, 110));
		scrollPane = new JScrollPane(chatArea);		
		return scrollPane;
	}
	
	
	private void attachKeyListener (JTextField input) {
		input.addKeyListener(new KeyListener () {

			@Override 
			public void keyPressed(KeyEvent e) {}
			@Override 
			public void keyReleased(KeyEvent e) {}
			
			@Override 
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == 10) {
					sendMessage();
				}
				
			}
		
		});
	}
	
	private void attachSendAction (JButton sendButton) {
		sendButton.addActionListener(new ActionListener () {
			public void actionPerformed(ActionEvent action) {
				sendMessage();
			}
		});
		
	}
	
	private void sendMessage () {
		String userInput = userInputArea.getText();
		userInputArea.setText("");
		synchronized (userInputs) {
			userInputs.add(userInput);
		}
	}
	
	public ArrayList<String> getUserInputs () {
		ArrayList<String> inputs = new ArrayList<String>();
		synchronized (userInputs) {
			inputs.addAll(userInputs);
			userInputs.clear();
		}
		return inputs;
	}
	
	
	public void updateChatArea (final String msgs) {
		
		SwingUtilities.invokeLater(new Runnable () {
			public void run () {
				chatArea.setText(msgs);				
				JScrollBar vertical = scrollPane.getVerticalScrollBar();
				vertical.setValue(vertical.getMaximum());
				repaint();	
			}
		});
	}
			
}
