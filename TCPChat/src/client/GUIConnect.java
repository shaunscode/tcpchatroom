package client;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUIConnect extends JPanel {

	private JTextField name;
	private JTextField address;
	private JTextField port;
	private JLabel label;
	private JLabel errorMessage;
	private JButton connect;
	private JButton exit;
	private Object clientLock;
	
	public GUIConnect (Object clientLock) {
		
		this.clientLock = clientLock;
		
		this.name = new JTextField(20);
		this.address = new JTextField(20);
		this.port = new JTextField(20);
		this.connect = new JButton("Connect");
		this.exit = new JButton("Exit");
		this.errorMessage = new JLabel();
		
		this.setLayout(new GridBagLayout());
		
		buildLayout();
		
		this.addButtonListeners();
	}
	
	private void buildLayout () {
		GridBagConstraints c = new GridBagConstraints();

		label = new JLabel("Display name:");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(10, 10, 10, 10);
		this.add(label, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 0;
		c.insets = new Insets(10, 10, 10, 10);
		this.add(name,  c);
		
		label = new JLabel("Server IP:");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(10, 10, 10, 10);
		this.add(label, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 1;
		c.insets = new Insets(10, 10, 10, 10);
		this.add(address, c);
		
		label = new JLabel("Server port:");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 2;
		c.insets = new Insets(10, 10, 10, 10);
		this.add(label, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 2;
		c.insets = new Insets(10, 10, 10, 10);
		this.add(port, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 2;
		c.insets = new Insets(10, 10, 10, 10);
		this.add(errorMessage, c);		
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 4;
		c.insets = new Insets(10, 10, 10, 10);
		this.add(connect, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 5;
		c.insets = new Insets(0, 10, 10, 10);
		this.add(exit, c);
		
	}
	
	private void addButtonListeners () {
		connect.addActionListener(new ActionListener () {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (validateInput()) {
					synchronized (clientLock) {
						clientLock.notify();
					}
				} 
			}
		});
		
		exit.addActionListener(new ActionListener () {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
	}
	
	private boolean validateInput () {
		String nameInput = name.getText();
		name.setText(nameInput.replace(" ", ""));	//remove spaces from names
		
		if (nameInput.length() <= 0) {
			errorMessage.setText("Error: Must enter a display name");
			return false;
		}
		
		
		String serverInput = address.getText();
		System.out.println("Add: " + serverInput);
		String[] nums = serverInput.split("\\.");	//need to escape the period sign		
		if (nums.length != 4 && !serverInput.toLowerCase().trim().equals("localhost")) {
			errorMessage.setText("Error: bad server ip");
			return false;
		}
		
		try {
			Integer.parseInt(port.getText());
		} catch (NumberFormatException e) {
			errorMessage.setText("Error: bad server port");
			return false;
		}
		
		return true;
	}
	
	public String getName () {
		return name.getText();
	}
	
	public String getServerAddress () {
		return address.getText();
	}
	
	public String getServerPort () {
		return port.getText();
	}
	
}
