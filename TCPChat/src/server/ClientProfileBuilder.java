package server;
import java.net.Socket;
import java.util.ArrayList;

import common.Connection;
import common.NetProfile;

public class ClientProfileBuilder {
	
	private ArrayList<NetProfile> waitingClients = new ArrayList<NetProfile>();	
		
	public void buildClient (Socket socket) throws Exception{
    	
		Connection newClientConnection = buildClientConnection(socket);	
		NetProfile newClient = buildNetProfile(newClientConnection);
    	
    	synchronized (waitingClients) {
    		waitingClients.add(newClient);
    	}
    	
    }
	
	private NetProfile buildNetProfile (Connection connection) throws Exception {
		NetProfile newClient = new NetProfile (connection); 	
		return newClient;		
	}
	
	
	private Connection buildClientConnection (Socket newClientSocket) throws Exception {
		return new Connection ("Server" , newClientSocket);
	}
	
	
	public ArrayList<NetProfile> getWaitingClients () {
		
		ArrayList<NetProfile> newClients = new ArrayList<NetProfile>();
		synchronized (waitingClients) {			
			newClients.addAll(waitingClients);
			waitingClients.clear();
		}

		return newClients;
	}
	
}
