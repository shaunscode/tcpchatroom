/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import common.RunningThread;

/**
 *
 * @author sac248
 */
public class ConnectionListener extends RunningThread {
	
	private ClientProfileBuilder connectionHandler;	
    private ServerSocket serverSocket;
    private int listeningPort;
    
    
    public ConnectionListener(ClientProfileBuilder handler) {
		super("Server Connection Listener");
		connectionHandler = handler;
	}
    
    public void setPort (int port) {
    	this.listeningPort = port;
    }
    
    public void run() {
        
    	int startingAttemps = 0;
    	
    	try {
			serverSocket = new ServerSocket(listeningPort);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
        while (isRunning() && startingAttemps < 5) {
        	
        	try {
            	
            	Socket incomingConnection = null;
                incomingConnection = serverSocket.accept();  
				connectionHandler.buildClient(incomingConnection);				
				
			} catch (Exception e) {
				startingAttemps++;
				System.out.println(this.getRunningThreadId() + ": Attemp " + startingAttemps + ", Exception at ConnectionListener trying to accept connection: " + e.getMessage());
				
				try {
					Thread.sleep(200);
				} catch (InterruptedException ie) {	}
				continue;
			} 
 
            startingAttemps = 0;
            
        }
        
        this.stopRunning();
        
    }
    

    @Override
    public void stopRunning() {
        try {
            serverSocket.close();
        } catch (SocketException e) {            
            ;
        } catch (IOException io) {
            ;
        } catch (NullPointerException n) { 
        	;
        } finally {
        	super.stopRunning();
        }
    }

    
}
