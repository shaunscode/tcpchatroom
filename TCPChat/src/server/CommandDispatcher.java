package server;

import java.util.ArrayList;

import common.Command;
import common.NetProfile;

public class CommandDispatcher {
	
	private ArrayList<NetProfile> clients;
	
	public CommandDispatcher (ArrayList<NetProfile> clients) {
		this.clients = clients;
	}
		
	public void dispatchCommands (ArrayList<Command> cmds) {
		for (Command cmd : cmds) {
			if (cmd.to != null) {
				dispatchCommand(cmd.to, cmd);
				continue;
			}
			for (NetProfile client : clients) {	
				dispatchCommand(client, cmd);
			}	
		}
	}
	
	private void dispatchCommand (NetProfile client, Command command) {
		client.sendNewCommand(command);
	}
	
	
}
