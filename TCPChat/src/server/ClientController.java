package server;

import java.util.ArrayList;

import common.Constants;
import common.NetProfile;
import common.Command;
import common.CommandProducer;
import common.CommandType;

public class ClientController extends CommandProducer {

	private ArrayList<NetProfile> clients = new ArrayList<NetProfile>();

	public void handleNewClients(ArrayList<NetProfile> newClients) {
		clients.addAll(newClients);	
		
		for (NetProfile client: newClients) {
			String msg;
			msg = "<Client " + client.getDisplayName() + " has connected>";
			sendCommand(new Command(CommandType.PRINT, msg));
			
			msg = buildConnectedList();
			sendCommand(new Command(CommandType.UPDATE_CONNECTED, msg));
			System.out.println("Server.ClientController: " + clients.size() + " clients connected..");
		}
		//System.out.println("Server.ClientController: " + clients.size() + " clients connected..");
	}

	private String buildConnectedList () {
		StringBuilder sb = new StringBuilder();
		for (NetProfile client: clients) {
			sb.append(client.getDisplayName() + ",");
		}
		return sb.toString();
	}
	
	public ArrayList<NetProfile> getClients () {
		return clients;
	}
	
	public ArrayList<Command> getNewCommandsIn () {
		ArrayList<Command> newCommandsReceived = new ArrayList<Command>();
		for (NetProfile client : clients) {
			ArrayList<Command> clientsNewCommands = client.getNewCommands();
			for (Command cmd: clientsNewCommands) {
				cmd.from = client;
			}	
			newCommandsReceived.addAll(clientsNewCommands);
		}
		return newCommandsReceived;
	}
	
	
	public void cleanUpClients () {
		for (NetProfile client : clients) {
			if (System.currentTimeMillis() > client.getLastAck() + Constants.SERVER_TIMEOUT_THRESHHOLD) { 
				client.sendNewCommand(new Command(CommandType.DISCONNECT));
				client.closeConnection();
				
			}
		}
	}
	
	public void pingClients() {

		long currentTime = System.currentTimeMillis();
		
		for (NetProfile client : clients) {
			if (currentTime > client.getLastPing() + Constants.PING_FREQUENCY) { 
				client.sendNewCommand(new Command(CommandType.PING));
				client.setLastPing(currentTime);
			}
		}
	}
		
	public void  closeAllClientConnections() {
		for (NetProfile client: clients) {		
			client.closeConnection();			
		}
	}
	
}
