package server;

import java.util.ArrayList;

import common.Constants;
import common.Command;

public class Server {
	
	
	private ConnectionListener connectionListener;
	private ClientProfileBuilder connectionBuilder;
	
	private ClientController clients;
	
	private CommandProcessor commandsProcessor;
	private CommandDispatcher commandsDispatcher;
	
	ArrayList<Command> commandsToSend;

    public Server(int port) {
    	
    	
    	clients = new ClientController();    	
    	connectionBuilder = new ClientProfileBuilder();
    	connectionListener = new ConnectionListener(connectionBuilder);
    	
    	commandsProcessor = new CommandProcessor(clients.getClients());    	
    	commandsDispatcher = new CommandDispatcher(clients.getClients());    	
    	
    	startConnectionListener(port);
    	System.out.print("\nServer: Waiting for clients..");
    	
        while (connectionListener.isRunning()) {       	
        	
        	//wont forget to clear each loop when created here
        	ArrayList<Command> newCommandsIn = new ArrayList<Command>();
        	ArrayList<Command> commandsToDispatch = new ArrayList<Command>();    		
        	
        	//handle new clients
        	clients.handleNewClients(connectionBuilder.getWaitingClients());			
			
        	//get cmds from clients and process the commands
        	newCommandsIn = clients.getNewCommandsIn();
			commandsProcessor.processCommands(newCommandsIn);
			
			//get commands to go out from varies places and send
			commandsToDispatch.addAll(commandsProcessor.getCommandsToDispatch());
			commandsToDispatch.addAll(clients.getCommandsToDispatch());
			commandsDispatcher.dispatchCommands(commandsToDispatch);
			
			//other
			clients.pingClients();
			
			try {
				Thread.sleep(Constants.SERVER_REFRESH_RATE);
			} catch (InterruptedException e) {}
        }
        
        
        clients.closeAllClientConnections();
        connectionListener.stopRunning();
       
        System.out.println("Server: Server main loop ended. Server program ended.");
        
    }
    
    
    private void startConnectionListener (int port) {       

    	connectionListener.setPort(port);
    	connectionListener.startRunning();
    	
        while (!connectionListener.isRunning()) {
        	System.out.println("Server: waiting for connection listener to start...");
        }
        
    }
    

}