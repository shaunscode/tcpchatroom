package server;

import common.CommandProducer;

import java.util.ArrayList;

import common.Command;
import common.CommandType;
import common.NetProfile;

public class CommandProcessor extends CommandProducer {
	
	private ArrayList<NetProfile> clients;
	
	public CommandProcessor (ArrayList<NetProfile> clients) {
		this.clients = clients;
	}

	public void processCommands (ArrayList<Command> msgs) {		
		//process them all 
		for (Command msg: msgs) {
			processCommand(msg);
		}			
	}
	
	private void processCommand (Command cmd) {
		switch (cmd.type) {
		case CL_INPUT:
			//System.out.println("Server.ClientController.processMessage(); Processing: " + cmd + " at " + System.currentTimeMillis());		
			String message = cmd.from.getDisplayName() + ": " + cmd.args;
			sendCommand(new Command(CommandType.PRINT, message));
			break;				
		case PING:
			updateClientAck(cmd.from);
			break;
		case NAME:
			updateClientName(cmd.from, cmd.args); 	//no spaces in name, get first work only.
			break;
		case STATS:
			sendStatsToClient(cmd.from);
			break;
		case HELP:
			sendHelpDetailsToClient(cmd.from);
			break;
		case MSG:
		case MESSAGE:
			sendWhisperMessage(cmd);
			break;
		default:
			System.out.println("Server.ClientController.processMessage(): Unhandled message: " + cmd);
			break;		
		}		
	}
	
	private void sendWhisperMessage(Command cmd) {
		
		//get person to whisper to
		String[] tokens = cmd.args.split(" ");
		NetProfile client = getClientByName(tokens[0].trim());
		Command newCmd;
		
		if (client == null) {
			newCmd = new Command(CommandType.PRINT, "<Unable to find '" + tokens[0].trim() + "' in the client list.>");
			newCmd.to = cmd.from;			
		} else {		
			//get the msg to send minus the 'to' name at the start.
			int index = tokens[0].length() + 1;
			String message = "(" + cmd.from.getDisplayName() + " whispers.. " + cmd.args.substring(index).trim() + ")";
			newCmd = new Command(CommandType.PRINT, message);
			newCmd.to = client;
		}
		
		sendCommand(newCmd); 
		
	}

	private NetProfile getClientByName (String name) {
		for (NetProfile client: clients) {
			if (name.equalsIgnoreCase(client.getDisplayName())) {
				return client;
			}
		}
		return null;
	}
	private void sendToClient (String message, NetProfile client) {
		Command cmd = new Command(CommandType.PRINT, message);
		cmd.to = client;
		sendCommand(cmd);
	}
	
	private void sendHelpDetailsToClient (NetProfile toClient) {
		String help = "==========\nHelp\n==========\n" +
				"use '/' infront of commands in the text input field followed by arguments\n" +
				"\teg. /name newName\n" +
				"Available commands are;\n" +
				"\t/help\n" +
				"\t/name [name]\n" +
				"\t/msg|message [name] [message to person]\n";
		sendToClient(help, toClient);
				
	}
	
	private void sendStatsToClient (NetProfile toClient) {
		StringBuilder sb = new StringBuilder();
		sb.append("==========\nServer stats\n==========\nCurrently connected...\n");
		for (NetProfile client: clients) {
			sb.append("\t" + client.getDisplayName() + "\n");
		}
		sendToClient(sb.toString(), toClient);
	}
	
	private void updateClientName (NetProfile client, String name) {
		String prevName = client.getDisplayName();
		client.setClientName(name);
		String notice = "<Client " + prevName + " is now known as " + name + ">";
		sendCommand(new Command(CommandType.PRINT, notice));
	}
	
	private void updateClientAck (NetProfile client) {
		client.setLastAck(System.currentTimeMillis());
	}
		
	
}
