package common;

import java.io.IOException;
import java.util.ArrayList;

public class NetProfile {

	//private Connection connection;
	private Network net;
	private String displayName = "unnamed";
	
	private long lastPing;
	private long lastAck;	
	
	public NetProfile (Connection conn) {
		
		this.net = new Network(conn);
		this.lastPing = System.currentTimeMillis();
		this.lastAck = System.currentTimeMillis();
	}
	
	public Network getNetwork () {
		return net;
	}
	
	public boolean isConnected () {
		return net.isConnected();
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setClientName(String displayName) {
		this.displayName = displayName;
	}

	
	public void closeConnection () {
		try {
			net.shutdown();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public ArrayList<Command> getNewCommands () {
		return net.getCommands();
	}

	public void sendNewCommand (Command cmd) {
		net.sendCommand(cmd);
	}
	

	public void pingSent () {
		lastPing = System.currentTimeMillis();
	}
	
	public void pingReceived () {
		lastAck = System.currentTimeMillis();
	}

	public long getLastPing() {
		return lastPing;
	}

	public void setLastPing (long time) {
		lastPing = time;
	}
	
	public long getLastAck() {
		return lastAck;
	}

	public void setLastAck(long time) {
		this.lastAck = time;
	}
		
	public long getRoundTime() {
		return System.currentTimeMillis() - lastAck;
	}
	
}
