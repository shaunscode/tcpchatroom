package common;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

public class ConnectionInput {

	private ObjectInputStream input;
	private ArrayList<NetMessage> msgs;
	private int port;
	
	private boolean debug = false;
	
	public ConnectionInput(Socket socket) throws IOException {

		this.port = socket.getPort();
		socket.setSoTimeout(Constants.NET_REFRESH_RATE);
		msgs = new ArrayList<NetMessage>();
		input = new ObjectInputStream(socket.getInputStream());

	}


	public void setDebug(boolean b) {
		this.debug = b;
	}
	
	public void doInputProcess() throws IOException, ClassNotFoundException {

		//Debug.print("input.doInput entered...");
		//Debug.print("\tmsgs size before: " + msgs.size());
		Object newObject = null;
		try {
			newObject = input.readObject();
			NetMessage newMsg = (NetMessage) newObject;
			System.out.println("\tconn in: read new msg: " + newMsg);
			synchronized (msgs) {
				//System.out.println("\tconn in: msg added: " + newMsg);
				msgs.add(newMsg);
			}
		} catch (SocketTimeoutException e) {
			; // do nothing as this is expected
		} catch (ClassCastException e) {
			System.out.println("ConnectionInput: bad cast to NetMessage! Shouldn't have happened. Msg: " + e.getMessage());
			System.out.println("ConnectionInput: object toString(): " + newObject.toString());
		}

		//Debug.print("\tmsgs size afer: " + msgs.size());
		//Debug.print("input.doInput ended.");
	}

	public synchronized ArrayList<NetMessage> getMessages() {
		//Debug.print("intput getMessages() entered...");
		//Debug.print("\tmsgs.size:  "+ msgs.size() + "");

		ArrayList<NetMessage> newMessages = new ArrayList<NetMessage>();
		newMessages.addAll(msgs);
		msgs.clear();
		
		//Debug.print("\tnewMsgs size:  "+ newMessages.size() + "");
		//Debug.print("\tmsgs.size after clearing:  "+ msgs.size() + "");

		//Debug.print("input getMessages ended.");
		return newMessages;
	}

}
