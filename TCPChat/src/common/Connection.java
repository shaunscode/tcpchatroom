/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author sac248
 */
public class Connection extends RunningThread {
	
	public Socket socket;

	private ConnectionInput input;
	private ConnectionOutput output;

	public Connection(String name, Socket socket) throws IOException {
		super(name);
		this.socket = socket;
		this.output = new ConnectionOutput(socket);
		this.input = new ConnectionInput(socket);

		System.out.println(this.getRunningThreadId() + " Connection: port: " + socket.getPort());
		
		this.startRunning();
		System.out.println(name + "Connection successfully running");
		
	}

	
	public ArrayList<NetMessage> getMessages() {
		return input.getMessages();
	}

	public void sendMessage(NetMessage netMessage) {
		output.sendMessage(netMessage);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		while (isRunning()) {

			try {
				//System.out.println(this.getRunningThreadId() + " calling doOutputProcess()..");
				output.doOutputProcess();
			} catch (IOException e) {
				System.out.println(this.getRunningThreadId() + ": doOutputProcess() threw IOException: " + e.getMessage());
				stopRunning();
			}
			
			try {
				//System.out.println(this.getRunningThreadId() + " calling doInputProcess()..");
				input.doInputProcess();
			} catch (IOException e) {
				System.out.println(this.getRunningThreadId() + ": doInputProcess() threw IOException: " + e.getMessage());
				stopRunning();
			} catch (ClassNotFoundException e) {
				System.out.println(this.getRunningThreadId() + ": doInputProcess() threw ClassNotFoundException: " + e.getMessage());
				System.out.println(this.getRunningThreadId() + ": --> allowing to continue running..");
			}

		}
		
		System.out.println(this.getRunningThreadId() + ": Closing connections and stopping connection thread.");		
		try {			
			closeConnection();
		} catch (IOException e) {
			System.out.println(this.getRunningThreadId() + ": closeConnection() threw IOExpcetion: " + e.getMessage());
		}

		System.out.println(this.getRunningThreadId() + ": Thread no longer active.");
	}

	public void closeConnection() throws IOException {		
		socket.shutdownInput();
		socket.shutdownOutput();	
	}

	public int getPort () {
		return socket.getPort();
	}
	
}
