package common;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class ConnectionOutput {

	
	private ObjectOutputStream output;
	private ArrayList<NetMessage> msgs;
	private int port = 0;
	
	public ConnectionOutput(Socket socket) throws IOException {

		port = socket.getPort();
		System.out.println("ConnectionOutput port: " + port);
		msgs = new ArrayList<NetMessage>();
		output = new ObjectOutputStream(socket.getOutputStream());
		output.flush();
	
	}
	
	

	public void doOutputProcess () throws IOException {		
		//Debug.print("output.doOutput entered..");
	
		synchronized (msgs) {
			//Debug.print("\tmsgs size  before sending: " + msgs.size());
			while (msgs.size() > 0) {

				NetMessage msg = msgs.get(0);
				System.out.println("\tconn out: writing msg: " + msg);
				output.writeObject(msg);
				output.flush();
				msgs.remove(0);

			}
			//Debug.print("\tmsgs.size() after sending: " + msgs.size());
		}	

		//Debug.print("output.doOutput ended");

	}

	public void sendMessage(NetMessage msg) {
		synchronized (msgs) {
			msgs.add(msg);
		}
	}
	
}
