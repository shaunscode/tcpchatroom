package common;

public class Debug {

	public static boolean debug = false;
	
	public static void print (String s) {
		if (debug) {
			System.out.println(s);
		}
	}
	
}
