/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.io.Serializable;

/**
 *
 * @author sac248
 */
public class Command implements Comparable<Command> {
    
	public CommandType type;
	public String args;
    public long created;
    
    public NetProfile from;
    public NetProfile to;	//
    
    public Command (CommandType type) {
    	this(type, "");	//avoid index out of bounds exceptions from null array
    }
    
    public Command (CommandType type, String args) {
    	this.type = type;
    	this.args = args;
    	this.created = System.currentTimeMillis();
    }
    
	@Override
	public int compareTo(Command other) {
		return (this.created > other.created)? 1: -1;
	}
	
	public String toString () {
		return "type/args: " + type + "/" + args;
	}
    
}
