package common;

import java.io.Serializable;

public class NetMessage implements Serializable {

	/**
	 * 	purely for sending over the net as a string. let Network class convert to/from Commands
	 */
	private static final long serialVersionUID = 1L;

	private String message;
	
	public NetMessage (String msg) {
		this.message = msg;
	}
	
	public String getMessage () {
		return message;
	}
	
	public String toString () {
		return message;
	}
}
