package common;

public class Constants {

	public final static int NET_REFRESH_RATE = 50;	//how often the net connection checks/sends messages
	public final static int SERVER_REFRESH_RATE = 50;	
	public final static int CLIENT_REFRESH_RATE = 50; 
	public final static int PING_FREQUENCY = 5000; //if cl/sv refresh any higher than this there will be no point, they will always be ready to ping again next refresh
	public final static int SERVER_TIMEOUT_THRESHHOLD = 30000;	//half a minute
	
	
	
}
