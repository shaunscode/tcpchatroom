package common;

import java.io.IOException;
import java.util.ArrayList;

public class Network {

	private Connection connection;
	
	public Network (Connection conn) {
		this.connection = conn;
	}
	
	public void shutdown () throws IOException {
		connection.closeConnection();
	}
	
	public boolean isConnected () {
		return connection.isRunning();
	}
	
	public ArrayList<Command> getCommands() {
		ArrayList<Command> cmds = new ArrayList<Command>();
		for (NetMessage msg: connection.getMessages()) {
			cmds.add(createCommandFromMessage(msg));
		}		
		return cmds;	
		
	}

	public void sendCommand(Command cmd) {
		NetMessage msg = createMessageFromCommand(cmd);		
		connection.sendMessage(msg);
	}
	
	private Command createCommandFromMessage (NetMessage msg) {
		int index = msg.getMessage().indexOf(" ");
		String cmdString = msg.getMessage().substring(0, index);
		String args = msg.getMessage().substring(index + 1,  msg.getMessage().length());
		CommandType cmdType = CommandType.valueOf(cmdString);
		return new Command(cmdType, args);
	}
	
	private NetMessage createMessageFromCommand (Command cmd) {
		return new NetMessage(cmd.type + " " + cmd.args);
	}
	
	
}
