package common;

public abstract class RunningThread extends Thread {

	private boolean running;
	private boolean isShowingDebug;
	private String debugName;
	
	public RunningThread (String name) {
		this.debugName = name;
		isShowingDebug = true;
		System.out.println(debugName + ":: Created.");
	}
	
	public boolean isRunning () {
		return this.running;
	}
	
	public void startRunning () {
		this.running = true;
		if (isShowingDebug()) {
			System.out.println(debugName + ":: startRunning() called.."); 
		}
		super.start();
	}
	
	//force users of this class to include a run method
	public abstract void run ();
	
	public void stopRunning () {
		if (isShowingDebug()) {
			System.out.println(debugName + ":: stopRunning() called..");
		}
		this.running = false;
	}

	public boolean isShowingDebug () {
		return this.isShowingDebug;
	}
	
	public String getRunningThreadId () {
		return debugName;
	}

}