package common;

import java.util.ArrayList;

public abstract class CommandProducer {

	private ArrayList<Command> commandsToSend = new ArrayList<Command>();

	public ArrayList<Command> getCommandsToDispatch() {
		ArrayList<Command> cmds = new ArrayList<Command>();
		cmds.addAll(commandsToSend);
		commandsToSend.clear();
		return cmds;
	}
	
	public void sendCommand (Command cmd) {
		commandsToSend.add(cmd);
	}
	
	
	
	
}
