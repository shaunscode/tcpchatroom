package common;

public enum CommandType {
	VOID, 	//a nothing message
	PING, 	//generic between cl/sv ping 
	NAME, 	//cl to use to change name 	
	CL_INPUT, 	//text msg to server
	UPDATE_CONNECTED,
	STATS,
	PRINT, 	//boardcast to all clients
	HELP,
	MSG,
	MESSAGE,
	DISCONNECT 	//tell client they are being disconnected
};

